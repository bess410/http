import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.stream.Collectors;

public class HttpConnection {
    private String uri;
    private Map<String, String> headers;
    private Map<String, String> request;

    public HttpConnection(String uri, Map<String, String> headers, Map<String, String> request) {
        this.uri = uri;
        this.headers = headers;
        this.request = request;
    }

    public void getResponseGet() throws IOException {
        URL obj = new URL(uri);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // set request method
        con.setRequestMethod("GET");

        //add request header
        if (headers != null) {
            headers.forEach(con::setRequestProperty);
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String result = br.lines().collect(Collectors.joining("\n"));
            System.out.println(result);
        }

    }

    public void getResponsePost() throws IOException {
        URL obj = new URL(uri);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        // set request method
        con.setRequestMethod("POST");

        //add request header
        if (headers != null) {
            headers.forEach(con::setRequestProperty);
        }

        StringBuilder sb = new StringBuilder();
        if (request != null) {
            request.forEach((k, v) -> sb.append(k).append("=").append(v).append("&"));
        }
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(sb.toString());
        wr.flush();
        wr.close();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String result = br.lines().collect(Collectors.joining("\n"));
            System.out.println(result);
        }
    }
}
